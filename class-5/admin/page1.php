<?php
session_start();

if(isset($_SESSION['isAuthenticated']) && $_SESSION['isAuthenticated']){
    echo $_SESSION['username'];
} else {
    header('location: ./index.php');
}