<?php

// $this, constructor

class Calculator {
    public $number1 = 0; // visibility: public, protected, private
    public $number2 = 0;

    public function __construct($n1, $n2)
    {
        $this->number1 = $n1;
        $this->number2 = $n2;
    }

    public function sum() {
        //code to be execute
        echo $this->number1 + $this->number2;
    }
}

$cal = new Calculator(10, 15); // object: instance of a class

$cal->sum();

// echo $cal->number1;