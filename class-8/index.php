<!doctype html>
<html lang="en">
<?php include './includes/head.php';?>
<body>
<?php require_once './includes/navbar.php';?>
    <div class="container-fluid">
        <div class="row">
            <?php include './includes/sidebar.php';?>
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <?php include './home.php'; ?>
            </main>
        </div>
    </div>
    <?php include './includes/sidebar.php';?>
</body>
</html>
