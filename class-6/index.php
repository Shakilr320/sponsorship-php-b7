<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    session_start();
    $products = $_SESSION['products'];
    ?>
    <a href="./create.php">Add New</a>

    <?php 
        if(isset($_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
    ?>

    <table border="1">
        <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sl = 0;
            foreach($products as $key => $product){ ?>
             <tr>
                <td><?= ++$sl ?></td>
                <td><?= $product['id'] ?></td>
                <td><?= $product['title'] ?></td>
                <td>
                    <a href="./show.php?index=<?= $key?>">Show</a>
                    <a href="./edit.php?index=<?= $key?>">Edit</a>
                    <a href="./delete.php?index=<?= $key?>">Delete</a>
                </td>
            </tr>
            <?php } ?> 

        </tbody>
    </table>
</body>
</html>